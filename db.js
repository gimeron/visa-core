/***
 * БД визуальных закладок
 * Данные хранятся в таблицах в виде массива объектов
 * Главные таблицы:
 *   links   - таблица ссылок
 *   tags    - теги и их описания
 *   protos  - протоколы и их описания
 *   domains - домены с портами и их описаниями
 *   users   - пользователи, добавившие запись и их описание
 * 
 * Записи Links могут быть нормализованы, так и нет. При обнаружении такой записи
 * специальная функция отмечает запись к нормализации.
 * 
 * Type - запись указывающая, как поступать со ссылкой. Пожет менять стиль,
 * обработчик кликов или указания к движку (может стоять метка "к нормализации").
 * Так же может содержать дополнительные технические данные о ссылке.
 **/

function VisADB() {
  this.dbStruct = {
    links: [],
    links_template: {
      proto: "proto|protoID", domain:"domain|domainID", query:"query", 
      type:"type", title:"title", text:"text", tags:"tags|tagsIDs", 
      user:"user|userID", description:"description"
    },
    tags:[],
    tags_template: ["tagID","tag","description"],
    users: [],
    users_template: ["userID","userName","description"],
    protos: [],
    protos_tamplate: ["protoID","protoName","description"],
    domains: [],
    domain_tamplate: ["domainID","domain","description"],
    next_linkID: 0,
    next_protoID: 0,
    next_domainID: 0,
    next_tagID: 0,
    next_userID: 0,
  };
  
  this.addLink = function(objLink) {
    //TODO
    return "linkID";
  };
  
  this.verifyLink = function(linkID) {
    //TODO
    return "linkIDs[] for verify";
  };
  
  this.removeLink = function(linkID) {
    //TODO
    return "true|false";
  };

  this.addProto = function(objProto) {
    //TODO
    return "protoID";
  };
    
  this.removeProto = function(protoID) {
    //TODO
    return "true|false";
  };

  this.addDomain = function(objDomain) {
    //TODO
    return "domainID";
  };
  
  this.removeDomain = function(domainID) {
    //TODO
    return "true|false";
  };

  this.addTag = function(objTag) {
    //TODO
    return "tagID";
  };
  
  this.removeTag = function(tagID) {
    //TODO
    return "true|false";
  };

  this.addUser = function(objUser) {
    //TODO
    return "userID";
  };
  
  this.removeUser = function(userID) {
    //TODO
    return "true|false";
  };

  
};

db = new VisADB();